#ifndef SENSORS_H
#define SENSORS_H

#include "globals.h"

//
//    Using the QTRSensors.h library version 3.1.0
//

//some consts
const int valuesPerSec = 30;
const float delayPerValue = (1000/valuesPerSec);


//QTRX sensor instance
extern QTRSensorsRC qtr;
#define NUM_IR 6
#define TIMEOUT 2500
extern float irFilteredValues[NUM_IR+1];
extern unsigned int irRawValues[NUM_IR];
extern unsigned int irRawPosition;

//ultrasonic sensors
#define NUM_US 10
extern float usFilteredValues[9];

extern int filter(int value, float* lastValue);

extern int readUsSensor(int trigPin, int echoPin);

void readUS(bool filterValues = true);

void readIR();

void sendData();

void printData();

/**
 * @description: recalibrates the QTRX module
*/
void calibrateQTRX();


#endif