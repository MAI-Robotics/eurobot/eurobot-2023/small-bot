#ifndef PINS_H
#define PINS_H

//right us sensor pins
#define US_RIGHT_TRIG 2
#define US_RIGHT_BACK_ECHO 3
#define US_RIGHT_ECHO 4
#define US_RIGHT_FRONT_ECHO 5

//left us sensor pins
#define US_LEFT_FRONT_ECHO 6
#define US_LEFT_ECHO 7
#define US_LEFT_BACK_ECHO 8
#define US_LEFT_TRIG 9

//trig for all lower us sensors
#define US_LOWER_TRIG 17
//back us sensor pins
#define US_BACK_LEFT_ECHO 18
#define US_BACK_RIGHT_ECHO 19
//front us sensor pins
#define US_FRONT_LEFT_ECHO 20
#define US_FRONT_RIGHT_ECHO 21

//QTRX sensor pins
#define QTRX_CTRL 22
#define QTRX_DT1 10
#define QTRX_DT2 11
#define QTRX_DT3 12
#define QTRX_DT4 13
#define QTRX_DT5 14
#define QTRX_DT6 15


#endif