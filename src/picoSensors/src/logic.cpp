#include "logic.h"
#include "pins.h"

void setPins() {
    //left us sensor setup
    pinMode(US_LEFT_TRIG, OUTPUT);
    pinMode(US_LEFT_ECHO, INPUT);
    pinMode(US_LEFT_FRONT_ECHO, INPUT);
    pinMode(US_LEFT_BACK_ECHO, INPUT);
    //right us sensor setup
    pinMode(US_RIGHT_TRIG, OUTPUT);
    pinMode(US_RIGHT_ECHO, INPUT);
    pinMode(US_RIGHT_FRONT_ECHO, INPUT);
    pinMode(US_RIGHT_BACK_ECHO, INPUT);
    //lower us trig setup  
    pinMode(US_LOWER_TRIG, OUTPUT);
    //front us sensor setup
    pinMode(US_FRONT_LEFT_ECHO, INPUT);
    pinMode(US_FRONT_RIGHT_ECHO, INPUT);
    //back us sensor setup
    pinMode(US_BACK_LEFT_ECHO, INPUT);
    pinMode(US_BACK_RIGHT_ECHO, INPUT);

    pinMode(25,OUTPUT);
}