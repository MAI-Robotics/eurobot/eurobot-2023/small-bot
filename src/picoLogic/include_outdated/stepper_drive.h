int stepsAll=1000;
int stepsLeft=100;
int stepsRight=100; 
int speedAll=500;
//calibration
const int calibrationValue = 47;
const float calibrationValueTurn = (68.0/8.75); 

enum Motor {LEFT, RIGHT}; 
enum Direction {CW, CCW}; //CW: clockwise; CCW: counterclockwise
enum DriveDirection {FW, BW}; //forwards; backwards
enum TurnDirection {L, R}; //L: Left; R: Right


/**
 * @description: tells the motors what to do
 * @param motor: LEFT/RIGHT
 * @param direction: CW(clockwise)/CCW(counterclockwise)
*/
void motorDirection(Motor motor, Direction direction) {
  if(motor == LEFT)  { //if left motor (TB6560)
    if(direction == CW) {
      digitalWrite(dirPinLeft, HIGH); //HIGH: clockwise
    }
    if(direction == CCW) {
      digitalWrite(dirPinLeft, LOW); //LOW: counterclockwise
    }
  } else if(motor == RIGHT) { //if right motor (TB6600)
    if(direction == CW) {
      digitalWrite(dirPinRight, LOW); 
    }
    if(direction == CCW) {
      digitalWrite(dirPinRight, HIGH); 
    }
  }
}

void oneStepBoth() { //steps on both motors
  digitalWrite(stepPinLeft, HIGH);
  digitalWrite(stepPinRight, HIGH);
  delayMicroseconds(speedAll);
  digitalWrite(stepPinLeft, LOW);
  digitalWrite(stepPinRight, LOW);
  delayMicroseconds(speedAll);
}

void multipleStepsBoth(int steps) {
  for (int i = 0; i < steps; i++) {
    oneStepBoth();
  }
}

void oneStepLeft() { //steps on left motor
  digitalWrite(stepPinLeft, HIGH);
  delayMicroseconds(speedAll);
  digitalWrite(stepPinLeft, LOW);
  delayMicroseconds(speedAll);
}

void oneStepRight() { //steps on right motor
  digitalWrite(stepPinRight, HIGH);
  delayMicroseconds(speedAll);
  digitalWrite(stepPinRight, LOW);
  delayMicroseconds(speedAll);
}


//----

/**
 * @description: drives a few centimeters
 * @param direction: FW/BW (forward/backwards)
 * @param length: centimeters
*/
void driveCentimeters(DriveDirection direction, int length) { //centimeters
  if(direction == FW) {
    motorDirection(LEFT, CCW);
    motorDirection(RIGHT, CW);
    int steps = length * calibrationValue;
    multipleStepsBoth(steps);
  }
  else if(direction == BW) {
    motorDirection(LEFT, CW);
    motorDirection(RIGHT, CCW);
    int steps = length * calibrationValue;
    multipleStepsBoth(steps);
  }
}

void driveCentimetersForward(int length) {
  driveCentimeters(FW, length);
}
void driveCentimetersBackwards(int length) {
  driveCentimeters(BW, length);
}

/**
 * @description: drives a few meters
 * @param direction: FW/BW (forward/backwards)
 * @param length: meters
*/
void driveMeters(DriveDirection direction, int length) { //meters
  if(direction == FW) {
    motorDirection(LEFT, CCW);
    motorDirection(RIGHT, CW);
    int steps = length * calibrationValue * 100;
    multipleStepsBoth(steps);
  }
  else if(direction == BW) {
    motorDirection(LEFT, CW);
    motorDirection(RIGHT, CCW);
    int steps = length * calibrationValue * 100;
    multipleStepsBoth(steps);
  }
}

void driveMetersForward(int length) {
  driveMeters(FW, length);
}
void driveMetersBackwards(int length) {
  driveMeters(BW, length);
}


/**
 * @description: turns bot
 * @param direction: L/R (left/right)
 * @param length: degrees
*/
void turnDegrees(TurnDirection direction, int degrees) {
  if(direction == L) { //linksdrehung
    motorDirection(LEFT, CCW);
    motorDirection(RIGHT, CCW);
    int steps = (int)(degrees * calibrationValueTurn); //converted to int
    multipleStepsBoth(steps);
  }
  else if(direction == R) {
    motorDirection(LEFT, CW);
    motorDirection(RIGHT, CW);
    int steps = (int)(degrees * calibrationValueTurn); //converted to int
    multipleStepsBoth(steps);
  }
}

void turnLeft(int degrees = 90) {
  turnDegrees(L, degrees);
}

void turnRight(int degrees = 90) {
  turnDegrees(R, degrees);
}
