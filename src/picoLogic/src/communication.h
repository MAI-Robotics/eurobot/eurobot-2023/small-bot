#ifndef COMMUNICATION_H
#define COMMUNICATION_H
//begin of .h file

#include "globals.h"


const int STRING_SIZE = 120;
const int US_SENSORS = 10;
const int IR_SENSORS = 7;
const int arraySize = US_SENSORS + IR_SENSORS;

extern char inputString[STRING_SIZE]; //Initialized variable to store recieved data
extern long int outputArray[arraySize];

extern int usValues[US_SENSORS];
extern int irValues[IR_SENSORS];

extern uint16_t LINE_POSITION;

extern int distanceDriven;
extern bool left_detected;
extern bool center_detected;
extern bool right_detected;


void splitString();

void getSensorValues();

#endif