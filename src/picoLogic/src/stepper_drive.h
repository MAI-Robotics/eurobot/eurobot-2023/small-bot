#ifndef STEPPER_H
#define STEPPER_H
//begin of .h file

#include "globals.h"

#include "logic.h"

/* data */
/**** Some constants ****/
const float wheelDiameter = 70;         // mm
const float outerWheelDistance = 219.87; // mm
const float wheelWidth = 12.00;         // mm
const float stepsPerRev = 1034;     // Steps that are needed to do one revolution
// Calculate the values for later calculating the amount of steps
const float midWheelDistance = outerWheelDistance - wheelWidth;
const float distancePerStep = PI * wheelDiameter / stepsPerRev;               // mm
const float anglePerStep = 360 / ((PI * outerWheelDistance) / distancePerStep); //°
#define tPerStep (customSpeed ? (maxTperStep-minTperStep)-(speed*(minTperStep/100)) : 540) // minimal delay between the steps in µs (For 11V: 873, For 22V: 436) function to calculate for other voltages:f(x)=-39.727x + 1310, fastest speed witch 11V is 540

extern float distancePer360Rotation;    //mm

extern bool stop_driving;
extern bool teamDir;

enum Condition {Line, Switch, No}; //CW: clockwise; CCW: counterclockwise
enum Team {Green, Blue}; //CW: clockwise; CCW: counterclockwise
extern Team team;

extern float x_pos;
extern float y_pos;
extern float direction;

extern bool CoAEnabled;
 
/**
 * @description: turns a specific angle, then drives a specific distance
 * @param angle: degrees to turn
 * @param distance: distance to drive
*/
void driveTo(float angle, float distance, bool CoA, Condition condition = No);

/**
 * @description: drives to a specific coordinate (measured in mm) (shortest way)
 * @param x: x-coordinate to drive to
 * @param y: y-coordinate to drive to
*/
void driveTo(int x, int y, bool CoA = CoAEnabled, Condition condition = No);

/**
 * @description: turns to a specific angle
 * @param angle: angle to turn to
*/
void turnTo(float angle);

/**
 * @description: sets the new x-y-position without driving anywhere
 * @param x: x-coordinate to set to
 * @param y: y-coordinate to set to
 * @param newDirection: new direction to set to
*/
void setPos(int x, int y, float newDirection=direction);

/**
 * @description: resets position to starting point
*/
void resetPos();

/**
 * @description: performs a step on both stepper motors
*/
void oneStepBoth(int timePerStep, bool dir, bool trackDistance = false);

/**
 * @description: tracks distance. needs to run at every step if driving forwards or backwards. not needed for turning
*/
void trackDistance(bool dir);

/**
 * @description: drives either forward or backward a given amount of steps
 * @param steps: amount of steps to drive
 * @param dir: true(clockwise)/false(counterclockwise)
 * @param CoA: collision avoidance by US
*/
void driveFast(unsigned int steps, bool dir, bool CoA);

/**
 * @description: drives until it detects a line
 * @param steps: maximum steps to drive
 * @param distanceOffset: distance measured in mm to drive after seeing line, add ~5cm when measuring from end of line to end of bot
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
*/
void driveUntilLine(unsigned int steps, unsigned int distanceOffset, bool dir, bool CoA);


/**
 * @description: drives until it detects a line, no restriction
 * @param distanceOffset: distance measured in mm to drive after seeing line, add ~5cm when measuring from end of line to end of bot
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
*/
void driveMaxUntilLine(unsigned int distanceOffset, bool dir, bool CoA);


/**
 * @description: drives until it hits the cherry support or drives against a wall
 * @param steps: steps to drive
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
*/
void driveUntilSwitch(unsigned int steps, bool dir, bool CoA);

/**
 * @description: drives until it hits the cherry support or drives against a wall
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
 * @param driveFurther: option to toggle driving further to the wall to secure standing perfect
*/
void driveMaxUntilSwitch(bool dir, bool CoA, bool driveFurther = false);


/**
 * @description: drives a set amount of mm
 * @param distance: distance in mm to drive
 * @param distanceOffset: distance measured in mm to drive after seeing line
 * @param dir: true(forward)/false(backward)
 * @param condition: specifies a limit -> Switch/Line/No
 * @param CoA: collision avoidance by US
*/
void driveDistanceMM(unsigned int distance, unsigned int distanceOffset, bool dir, Condition condition, bool CoA);

/**
 * @description: tactic generating feature while turning
 * @param oldDir: planned direction to turn (when teamGreen)
*/
extern bool readTeam(bool oldDir);

/**
 * @description: turns either right or left a given amount of steps
 * @param steps: amount of steps to turn
 * @param distanceOffset: distance measured in mm to drive (forward) after turning
 * @param dir: true(clockwise)/false(counterclockwise) -> will be reversed if teamBlue()
*/
void turnFast(unsigned int steps, unsigned int distanceOffset, bool dir);

/**
 * @description: turns either right or left a given amount of steps, no team reversing
 * @param steps: amount of steps to turn
 * @param distanceOffset: distance measured in mm to drive (forward) after turning
 * @param dir: true(clockwise)/false(counterclockwise) 
*/
void turnFastNoTeam(unsigned int steps, unsigned int distanceOffset, bool dir);

/**
 * @description: rotates through a specified angle
 * @param degree: degrees to turn
 * @param dir: true(clockwise)/false(counterclockwise)
*/
void turnAngle(float degree, bool dir, bool teamLogic = true);

/**
 * @description: calibration of line-sensor through bot turning left and right constantly
*/
void calibrationRoutine();

/**
 * @description: follows the line until reached maxDistance
 * @param maxDistance: maximum distance to follow the line (measured in mm)
*/
void followLine(int maxDistance);

/**
 * @description: follows the line until line lost
*/
void followLineMax();

/**
 * @description: if angle is greater than 180 degrees it reduces it by 360 and other way round if smaller than -180 degrees, value of direction will be between -180 and 180
*/
void directRotation(float* angle);

/**
 * @description: if wanted to set position again to renew position, this will be needed on x/y depending on wall where bot drives to. for example, if driving to wall which is on y=0, i'd need to put this func into the y field of setPos(). 
 * Additionally, the following adjustments should be made based on the arena direction:
 * - 0: valueToChange should be set to x-botLenght/2          
 * - 90: valueToChange should be set to y-botLenght/2       
 * - 180: valueToChange should be set to x+botLenght/2     = -180
 * - 270: valueToChange should be set to y+botLenght/2     = -90
 * 
 * @param valueToChange: value to change, can be new x or new y value
 * @param arenaDir: can be 0, 90, 180, 270 (no negative numbers)
*/
int fixSetPos(int valueToChange, unsigned int arenaDir);

#endif