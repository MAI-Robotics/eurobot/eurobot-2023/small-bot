## Arena-Testcodes
| Folder                    | Description                                                                         |
|---------------------------|-------------------------------------------------------------------------------------|
| include/                  | Include-header-files                                                                |
| arena-test.cpp            | arena-test code, no success while testing, no updates                               |
| basket-test.cpp           | bot drives to cherry holder and to basket works @2023-03-23, only theory no tactics |
| drive-test.cpp            | working, just drive test until switch,  no updates for this code                    |
| gripper-angle-test.cpp    | test for fixing all gripper angles again and again and again...                     |