## Platformio test folder

Command for testing a specific test-project:
pio test -vvv --without-uploading --without-testing -f "test_main"

- -vvv: better output
- --without-uploading: doesnt upload to board
- --without-testing: remove pio test stuff
- -f "test_name": test-project to test (if not used every test project will be tested)