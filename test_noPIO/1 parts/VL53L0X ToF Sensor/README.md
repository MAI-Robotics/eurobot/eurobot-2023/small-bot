# VL53L0X ToF Sensor
This is a sensor like HC-SR04-Ultrasonic sensors. It detects distance very well and will be used using I2C. We were planning on using this sensor at the front of the bot for detecting enemies.

## Pinout
| Pin number | Signal name | Signal type | Signal description |
| --- | --- | --- | --- |
| 1 | AVDDVCSEL | Supply | VCSEL Supply, to be connected to main supply |
| 2 | AVSSVCSEL | Ground | VCSEL Ground, to be connected to main ground |
| 3 | GND | Ground | To be connected to main ground |
| 4 | GND2 | Ground | To be connected to main ground |
| 5 | XSHUT | Digital input | Xshutdown pin, Active LOW |
| 6 | GND3 | Ground | To be connected to main ground |
| 7 | GPIO1 | Digital output | Interrupt output. Open drain output. |
| 8 | DNC | Digital input | Do Not Connect, must be left floating. |
| 9 | SDA | Digital input/output | I2C serial data |
| 10 | SCL | Digital input | I2C serial clock input |
| 11 | AVDD | Supply | Supply, to be connected to main supply |
| 12 | GND4 | Ground | To be connected to main ground |