#include "wiring.h"
#include "sensors.h"

void setup() {
    Serial.begin(9600);                 // Die serielle Kommunikation starten
    pinMode(TRIG, OUTPUT);           // Trigger Pin als Ausgang definieren
    pinMode(ECH, INPUT);               // Echo Pin als Eingang defnieren
}