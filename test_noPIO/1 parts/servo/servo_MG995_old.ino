// Source: https://microcontrollerslab.com/mg995-servo-motor-pinout-interfacing-with-arduino-features-examples/

#include <Servo.h> // Servo library by Arduino used https://www.arduino.cc/reference/en/libraries/servo/ (already integrated in Arduino IDE)
#define Servo_PWM 6 //Servo PWM Pin
Servo MG995_Servo;


void setup() {
  Serial.begin(9600); // Start Serial Monitor with 9600 Baud rate
  MG995_Servo.attach(Servo_PWM);  // Connect D6 of Arduino with PWM signal pin of servo motor
}

void loop() {
  Serial.println("0");// You can display on the serial the signal value
  MG995_Servo.write(0); //Turn clockwise at high speed
  delay(3000);
  MG995_Servo.detach();//Stop. You can use deatch function or use write(x), as x is the middle of 0-180 which is 90, but some lack of precision may change this value
  delay(2000);
  MG995_Servo.attach(Servo_PWM);//Always use attach function after detach to re-connect your servo with the board
  Serial.println("0");//Turn left high speed
  MG995_Servo.write(180);
  delay(3000);
  MG995_Servo.detach();//Stop
  delay(2000);
  MG995_Servo.attach(Servo_PWM);  
}
