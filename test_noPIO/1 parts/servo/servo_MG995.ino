// Sweep
// by BARRAGAN
#include <Servo.h>
Servo myservo;    // erzeugt ein Servomotor-Objekt
// maximal können acht Servomotor-Objekte erzeugt werden
int pos = 0;      // Variable, die die Servoposition (Winkel) speichert
void setup() {
  myservo.attach(9);  // an welchem Pin ist der Servomotor angeschlossen
}
void loop(){
  for(pos = 0; pos < 180; pos += 1) {  // von 0 bis 180 Grad, in Schritten von einem Grad
    myservo.write(pos);                   // sagt dem Servomotor, in welche Position sich drehen soll      
    delay(15);                            // wartet 15 Millisekunden   
  }    
  for(pos = 180; pos>=1; pos-=1) {     // und das gleiche zurück
    myservo.write(pos);
    delay(15);
  }
}