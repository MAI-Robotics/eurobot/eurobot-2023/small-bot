int readUsSensor(int trigPin, int echoPin){
    // Clears the TRIG_PIN
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    // Sets the TRIG_PIN on HIGH state for 10 micro seconds
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    // Reads the ECHO_PIN, returns the sound wave travel time in microseconds
    int duration = pulseIn(echoPin, HIGH);
    // Calculating the distance
    int distance = duration * 0.034 / 2;

    return distance;
}

void setup() {
    Serial.begin(115200); // Starts the serial communication

    //pinModes
    pinMode(2, OUTPUT);
    pinMode(3, INPUT);
    pinMode(4, INPUT);
    pinMode(5, INPUT);
    //pinMode(6, INPUT);
  
}
void loop() {

    Serial.print("1: ");
    Serial.println(readUsSensor(2, 3));
    delay(20); 
    Serial.print("2: ");
    Serial.println(readUsSensor(2, 4));
    delay(20); 
    Serial.print("3: ");
    Serial.println(readUsSensor(2, 5));
    delay(20); 
    Serial.print("4: ");
    Serial.println(readUsSensor(2, 6));
    delay(500);


    Serial.println("############################################");

}