//tested with QTRSensors.h version 3.1.0

#include <Arduino.h>
#include <stdlib.h>
#include "pins.h"
#include "servo_drive.h"
#include "communication.h"
#include "stepper_drive.h"
#include "logic.h"

void setup(){
    Serial.begin(9600);
    Serial1.begin(115200);
    pinMode(pullcord, INPUT_PULLUP);
    pinMode(team_select, INPUT_PULLUP);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    pinMode(25, OUTPUT);  //LED


    calibrationRoutine();
}

void loop() {
    getSensorValues();
    
    if((irValues[0] > 100) || (irValues[1] > 100) ||(irValues[2] > 100) || (irValues[3] > 100) || (irValues[4] > 100) || (irValues[5] > 100)) {
        digitalWrite(25, HIGH);
    } else() {
        digitalWrite(25, LOW);
    }

}