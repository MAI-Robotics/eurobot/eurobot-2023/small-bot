#include <QTRSensors.h>
#include "pins.h"

// us sensors
int usSensorCount = 10;

QTRSensors qtr;

const uint8_t irSensorCount = 6;
uint16_t irRawValues[irSensorCount];
float irFilteredValues[irSensorCount+1];

int filter(int value, float* lastValue){
    float a = 0.3;

    Serial.print(value);
    Serial.print(",");

    float filteredValue = ((1-a)*(*lastValue)) + (a*value);
    *lastValue = filteredValue;

    Serial.println(filteredValue);

    return filteredValue;

}


void setup()
{
  // configure the sensors
  qtr.setTypeRC();
  qtr.setSensorPins((const uint8_t[]){QTRX_DT1, QTRX_DT2, QTRX_DT3, QTRX_DT4, QTRX_DT5, QTRX_DT6}, irSensorCount);
  qtr.setEmitterPin(QTRX_CTRL);

  delay(500);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH); // turn on Arduino's LED to indicate we are in calibration mode

  // 2.5 ms RC read timeout (default) * 10 reads per calibrate() call
  // = ~25 ms per calibrate() call.
  // Call calibrate() 400 times to make calibration take about 10 seconds.
  for (uint16_t i = 0; i < 400; i++)
  {
    qtr.calibrate();
  }
  digitalWrite(LED_BUILTIN, LOW); // turn off Arduino's LED to indicate we are through with calibration

  // print the calibration minimum values measured when emitters were on
  Serial.begin(9600);
  for (uint8_t i = 0; i < irSensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.minimum[i]);
    Serial.print(' ');
  }
  Serial.println();

  // print the calibration maximum values measured when emitters were on
  for (uint8_t i = 0; i < irSensorCount; i++)
  {
    Serial.print(qtr.calibrationOn.maximum[i]);
    Serial.print(' ');
  }
  Serial.println();
  Serial.println();
  delay(1000);
}

void readIR() {
  // read calibrated sensor values and obtain a measure of the line position
  // from 0 to 5000 (for a white line, use readLineWhite() instead)
  uint16_t position = qtr.readLineBlack(irRawValues);

  // print the sensor values as numbers from 0 to 1000, where 0 means maximum
  // reflectance and 1000 means minimum reflectance, followed by the line
  // position
  for (uint8_t i = 0; i < irSensorCount; i++)
  {
    irFilteredValues[i] = irRawValues[i];
  }
  irFilteredValues[irSensorCount] = irRawPosition;

  delay(250);
}

void sendData(){

    for (int j = 0; j < irSensorCount; j++){
        Serial1.print(irFilteredValues[j]);
        Serial1.print(";");
    }

    Serial1.println(String(qtr.readLineBlack(irRawValues)));
}

void printData(){

    Serial.println("IR");

    for (int j = 0; j < irSensorCount; j++){
        Serial.print(j+1);
        Serial.print(": ");
        Serial.println(irFilteredValues[j]);
    }

    Serial.print("Position: ");

    Serial.println(String(qtr.readLineBlack(irRawValues)));

}