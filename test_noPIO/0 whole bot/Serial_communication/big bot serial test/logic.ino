#include <Arduino.h>
#include <stdlib.h>

const int STRING_SIZE = 70;

const int US_SENSORS = 6;
const int IR_SENSORS = 7;
const int arraySize = US_SENSORS + IR_SENSORS;


char inputString[STRING_SIZE]; //Initialized variable to store recieved data
long int outputArray[arraySize];

int usValues[US_SENSORS];
int irValues[IR_SENSORS];

void setup(){
    Serial.begin(115200); //connect to PC
    Serial.println("Started advanced receiver script");

}

void splitString(){ 
    int i = 0;
    char *p = strtok(inputString, ";");

    while(p != NULL) {
        Serial.print("Section found: ");
        Serial.println(p);

        outputArray[i++] = atof(p);
        p = strtok(NULL, ";");
    }
}


void getSensorValues(){
    
    splitString();

    Serial.print("US Sensor Values: ");
    for(int i = 0; i < US_SENSORS; i++){
        usValues[i] = outputArray[i];
        Serial.print(usValues[i]);
        Serial.print(", ");
    }
    Serial.println("");

    Serial.print("IR Sensor Values: ");
    for(int i = 0; i < IR_SENSORS; i++){
        irValues[i] = outputArray[i+US_SENSORS];
        Serial.print(irValues[i]);
        Serial.print(", ");
    }
    Serial.println("");
}

void loop() {
    while(Serial.available() <= 0){
        delay(100);
    }

    Serial.println("Data available");

    Serial.readStringUntil("\n").toCharArray(inputString, STRING_SIZE);
    Serial.println(inputString);

    getSensorValues();

    delay(50);
}

