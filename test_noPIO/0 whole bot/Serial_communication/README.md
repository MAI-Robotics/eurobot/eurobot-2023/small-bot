# Serial communication
Tests for our serial communication between two Arduino boards or two Raspberry Pi Picos.
This will help us to use multiple boards for more power and simultaneous actions.

## Images
![Wiring](examples/Serial-communication-between-Arduino.jpeg)
[Download Fritzing Diagram](https://1drv.ms/u/s!AjWxvMBValH5grwLT7QpMTgXhapBgA?e=ZH60QI)

