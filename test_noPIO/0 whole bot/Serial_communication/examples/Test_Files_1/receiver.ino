char data; // variable to store recieved data byte

void setup() {
  // Begin the Serial at 9600 Baud
  Serial.begin(9600);
}

void loop() {
  Serial.readBytes(&data,1); //Read the serial data and store in var
  Serial.println(data); //Print data on Serial Monitor
}
