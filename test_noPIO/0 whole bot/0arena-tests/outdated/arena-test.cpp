#include <Arduino.h>
//include files see "LogicPico/"
#include "pins.h"
#include "servo_drive.h"
#include "communication.h"
#include "stepper_drive.h"
#include "logic.h"

void setup(){
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    LEFT_SERVO.attach(left_servo);
    RIGHT_SERVO.attach(right_servo);
    MAIN_SERVO.attach(center_servo);

    pinMode(25, OUTPUT);
    delay(100);

    driveUntilSwitch(10000, true, false); //drive from Startzone to cherry holder
    openGripperBasket(); //Open gripper

    delay(100);
    lowerArm(); // put the gripper in the right position
    moveDown(); //Moves the gripper down lol
    closeGripper(); // grip the cherry

    delay(100);
    moveUp(); // moves the gripper up again
    driveDistanceMM(100, 0, false, 'n', false);
    turnAngle(180, true);
    driveMaxUntilSwitch(true, false);
    driveDistanceMM(140, 0, false, 'n', false);
    //driveDistanceMM(675, 1, false, 'n', false); // now hardcoded, will be converted to softcode
    turnAngle(90, false); // allign to basket

    delay(100);
    liftArm(); // puts the gripper in the right position
    driveUntilSwitch(10000, true, false); //drives to the basket
    openGripperBasket(); // puts the cherries into the basket
}
void loop(){
    //nothing in here
}

