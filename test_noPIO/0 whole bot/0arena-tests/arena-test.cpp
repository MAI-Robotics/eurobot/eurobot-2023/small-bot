#include "globals.h"
#include "pins.h"
#include "communication.h"
#include "logic.h"
#include "servo_drive.h"
#include "stepper_drive.h"
//#include "acceleration.h"

void setup(){
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    LEFT_SERVO.attach(left_servo);
    RIGHT_SERVO.attach(right_servo);
    MAIN_SERVO.attach(center_servo);

    pinMode(25, OUTPUT);
    delay(100);

    moveUp();
    lowerArm();
    closeGripper();

    for(digitalRead(pullcord) == LOW) { //wait till pull cord is triggered
        delay(5);
    }
    /*
    * start with bot looking towards cherry basket
    * test code for team green, for testing not yet for team blue (will be written after testing on wednesday in holidays)
    */

    //automatic homing
    turnAngle(90, true); //turn to wall
    driveMaxUntilSwitch(true, true); //drive to wall
    driveDistanceMM(225, 0, false, No, true);  //drive to middle of start zone
    turnAngle(90, false);



    driveDistanceMM(352, 100, true, Line, true);    //drives till bot stands half on line
    turnAngle(90, true);
    driveMaxUntilSwitch(true, true);               //drives till cherry holder

    openGripperBasket();
    lowerArm();
    moveDown();
    delay(1000);
    closeGripper();
    delay(100);
    moveUp(); 
    liftArm();          

    driveDistanceMM(100, 0, false, No, true);
    turnAngle(90, false);
    
    driveDistanceMM(550, 0, true, No, true);
    turnAngle(90, false);
    driveDistanceMM(365, 0, true, No, true);
    turnAngle(90, true);
    driveDistanceMM(645, 0, true, No, true);   //drives almost into zone

    turnAngle(90, true);
    driveDistanceMM(480, 0, true, Switch, true); //drives to wall
    driveDistanceMM(100, 0, false, No, true);
    turnAngle(90, false);

    driveDistanceMM(219, 0, true, Switch, true);   //drive to basket
    openGripperBasket();
    delay(3000);
    closeGripper();
    driveDistanceMM(100, 0, false, No, true);      //!!!!! if value not correct gripper might break
    lowerArm();

    turnAngle(90, false);

    moveUp();
    lowerArm();
    openGripperBasket();
    driveDistanceMM(820, 0, true, Switch, true);       //drive to second cherry holder
    moveDown();
    delay(1000);
    closeGripper();
    delay(1000);
    moveUp();
    liftArm();

    turnAngle(180, true);
    driveDistanceMM(1000, 0, true, Switch, true);
    driveDistanceMM(100, 0, false, No, true);
    turnAngle(90, false);
    driveMaxUntilSwitch(true, true);
    delay(1000);

    openGripperBasket();
    delay(2000);
    driveDistanceMM(100, 0, false, No, true);
    lowerArm();
    turnAngle(90, false);

    driveDistanceMM(150, 0, true, No, true);
    //bot in final zone

}
void loop(){
    //nothing in here
}

