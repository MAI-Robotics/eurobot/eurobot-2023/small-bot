#include "globals.h"
#include "pins.h"
#include "communication.h"
#include "logic.h"
#include "servo_drive.h"
#include "stepper_drive.h"
//#include "acceleration.h"

void setup(){
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);
    pinMode(pullcord, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    LEFT_SERVO.attach(left_servo);
    RIGHT_SERVO.attach(right_servo);
    MAIN_SERVO.attach(center_servo);

    pinMode(25, OUTPUT);
    delay(100);

    
/*
    driveUntilSwitch(10000, true, false);   //drive to cherry holder
    while(true) {
        delay(5);
    }
    delay(1000);
    moveDown();
    closeGripper();
    delay(1000);

    moveUp();
    liftArm();
    driveDistanceMM(100, 1, false, 'n', false);
    delay(1000);

    driveUntilSwitch(100000, true, false);  //drive to cherry basket
    delay(1000);
    openGripperBasket();
    delay(2000);

    closeGripper();
    driveDistanceMM(100, 239784, false, 'n', false);
    delay(1000);
    lowerArm();
    */
}
void loop(){
    if(digitalRead(pullcord)  == HIGH) {        //pull cord is low when plugged in
        driveDistanceMM(1000, 50, true, Line, false);
        while(true) {
            delay(5);
        }
    }
}

