#include "pins.h"
#include "drive.h"

void setup(){
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    pinMode(25, OUTPUT);

}

void loop(){
    moveUp();
    moveDown();
}