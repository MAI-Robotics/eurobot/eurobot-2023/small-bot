#include <Arduino.h>
#include <stdlib.h>
#include "pins.h"
#include "servo_drive.h"
#include "communication.h"
#include "stepper_drive.h"
#include "logic.h"

int dir;
unsigned int sensorValues[irSensorCount];

void driveFastDir(unsigned int steps, bool dir, bool CoA, bool stepper){      //stepper true: left, false: right  
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    for (unsigned int i = 0; i < steps; i++)
    {
        if(CoA)
          if(dir == true && (usValues[1] < 30 || usValues[4] < 30 || usValues[6] < 20 || usValues[7] < 20)){
              break;
          }
        if(stepper) {
            //digitalWrite(right_stepper_STEP, HIGH);
            digitalWrite(left_stepper_STEP, HIGH);
            delayMicroseconds(tPerStep);
            //digitalWrite(right_stepper_STEP, LOW);
            digitalWrite(left_stepper_STEP, LOW);
            delayMicroseconds(tPerStep);
        } else if(!stepper) {
            digitalWrite(right_stepper_STEP, HIGH);
            //digitalWrite(left_stepper_STEP, HIGH);
            delayMicroseconds(tPerStep);
            digitalWrite(right_stepper_STEP, LOW);
            //digitalWrite(left_stepper_STEP, LOW);
            delayMicroseconds(tPerStep);
        }


        digitalWrite(right_stepper_STEP, HIGH);
        digitalWrite(left_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep);
        digitalWrite(right_stepper_STEP, LOW);
        digitalWrite(left_stepper_STEP, LOW);
        delayMicroseconds(tPerStep);
    }
    delay(10);
}



void setup(){
    Serial.begin(9600);     //pc
    Serial1.begin(115200);  //serial
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);
    pinMode(pullcord, INPUT_PULLUP);
    pinMode(team_select, INPUT_PULLUP);

    //dc motor setup
    pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    LEFT_SERVO.attach(left_servo);
    RIGHT_SERVO.attach(right_servo);
    MAIN_SERVO.attach(center_servo);

    pinMode(25, OUTPUT);  //LED
    
    startUpRoutine();
}

void loop() {
    driveFast(10, true, false);
    getSensorValues();

    if((irValues[0] > 100)) {
        dir = 0;
    } else if((irValues[1] > 100)) {
        dir = 1;
    } else if((irValues[2] > 100)) {
        dir = 2;
    } else if((irValues[3] > 100)) {
        dir = 3;
    } else if((irValues[4] > 100)) {
        dir = 4;
    } else if((irValues[5] > 100)) {
        dir = 5;
    }

    if(dir <= 1) {  //drive left
        driveFastDir(100, true, false, false);
    } else if(dir >= 4) {  //drive right
        driveFastDir(100, true, false, true);
    }

    /*
    if((irValues[0] > 100) || (irValues[1] > 100) ||(irValues[2] > 100) || (irValues[3] > 100) || (irValues[4] > 100) || (irValues[5] > 100)){
        while(True) {
            delay(5);
        }
    }

    qtr.read(sensorValues);
    unsigned int position = qtr.readLine(sensorValues);

    int error = position - 2500;


    */
}