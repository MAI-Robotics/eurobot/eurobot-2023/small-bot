#include "main.h"

bool left_detected = false;
bool center_detected = false;
bool right_detected = false;

void setup(){
    Serial.begin(9600);
    Serial1.begin(115200);
    Serial.println("START");

    startUpRoutine();     //pins, gripper, waiting for pullcord (working)
    Serial.println("Done waiting");

    team = Green;           //not implemented, blue would be reversed -> needs testing
}

void loop() {
    if (irValues[0] < 900 || irValues[1] < 900) {
        left_detected = true;
    }
    if (irValues[2] < 900 || irValues[3] < 900) {
        center_detected = true;
    }
    if (irValues[4] < 900 || irValues[5] < 900) {
        right_detected = true;
    }


    if (center_detected) {              // line center detected
        driveFast(10, true, false);     // straight ahaid
    } else if (left_detected    && !right_detected) {  // line is left
        turnAngle(30, false); // links abbiegen
        driveDistanceMM(10, 0, true, No, false);
    } else if (!left_detected && right_detected) {  // line is right
        turnAngle(30, true); // rechts abbiegen
        driveDistanceMM(10, 0, true, No, false);
    } else { 
        driveDistanceMM(10, 0, false, No, false);
    }
}

void loop1() {
    Serial1.readStringUntil('\n').toCharArray(inputString, STRING_SIZE);
    getSensorValues();  //gets current sensor values from sensor pico
}