//limit switch pins
#define lower_rail_switch 13
#define upper_rail_switch 11

//dc motor pins
#define right_PWM 6
#define left_PWM 7

//servo pins
#define left_servo 18
#define right_servo 20
#define center_servo 21

//stepper
#define right_stepper_DIR 2
#define right_stepper_PUL 3
#define left_stepper_CW 4
#define left_stepper_CLK 5