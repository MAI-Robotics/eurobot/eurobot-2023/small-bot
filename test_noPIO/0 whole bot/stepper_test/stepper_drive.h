#include <math.h>

/* data */
/**** Some constants ****/
const float wheelDiameter = 70;         // mm
const float outerWheelDistance = 219.87; // mm
const float wheelWidth = 12.00;         // mm
const float stepsPerRev = 1036;     // Steps that are needed to do one revolution
float distancePer360Rotation = 589.5; //mm

// Calculate the values for later calculating the amount of steps
const float midWheelDistance = outerWheelDistance - wheelWidth;
const float distancePerStep = PI * wheelDiameter / stepsPerRev;               // mm
const float anglePerStep = 360 / ((PI * outerWheelDistance) / distancePerStep); //°

#define tPerStep 540 // minimal delay between the steps in µs (For 11V: 873, For 22V: 436) function to calculate for other voltages:f(x)=-39.727x + 1310

bool stop_driving = false;

/**
 * @description: drives either forward or backward a given amount of steps
 * @param steps: amount of steps to drive
 * @param dir: true(clockwise)/false(counterclockwise)
 * @param CoA: collision avoidance by US
*/
void driveFast(unsigned int steps, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    for (unsigned int i = 0; i < steps; i++)
    {
        getSensorValues();
        if(CoA) {
          if(dir == true && (usValues[1] < 30 || usValues[4] < 30 || usValues[6] < 20 || usValues[7] < 20)){
            break;
          }
          if(dir == false && (usValues[2] < 30 || usValues[5] < 30 || usValues[8] < 20 || usValues[9] < 20)){
            break;
          }
        }

        digitalWrite(right_stepper_STEP, HIGH);
        digitalWrite(left_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep);
        digitalWrite(right_stepper_STEP, LOW);
        digitalWrite(left_stepper_STEP, LOW);
        delayMicroseconds(tPerStep);
    }
    delay(10);
}

/**
 * @description: drives until it detects a line
 * @param steps: maximum steps to drive
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
*/
void driveUntilLine(unsigned int steps, unsigned int distanceOffset, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    float a = 0;

    for (unsigned int i = 0; i < steps; i++){
        getSensorValues();
        if(CoA) {
          if(dir == true && (usValues[1] < 30 || usValues[4] < 30 || usValues[6] < 20 || usValues[7] < 20)){
            break;
          }
          if(dir == false && (usValues[2] < 30 || usValues[5] < 30 || usValues[8] < 20 || usValues[9] < 20)){
            break;
          }
        }

        a = 0;

        if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
            a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
        }

        if (steps - i < 300 && i > steps / 2){  // decelerate to make stopping smoother
            a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
        }


        if((irValues[0] > 100) || (irValues[1] > 100) ||(irValues[2] > 100) || (irValues[3] > 100) || (irValues[4] > 100) || (irValues[5] > 100)){
          int stepsOffset = distanceOffset / outerWheelDistance * stepsPerRev;
          
          float a = 0;

          for (unsigned int i = 0; i < stepsOffset; i++){
              a = 0;

              if (steps - i < 300 && i > steps / 2){  // decelerate to make stopping smoother
                  a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
              }

              digitalWrite(right_stepper_STEP, HIGH);
              digitalWrite(left_stepper_STEP, HIGH);
              delayMicroseconds(tPerStep + a + 100);
              digitalWrite(right_stepper_STEP, LOW);
              digitalWrite(left_stepper_STEP, LOW);
              delayMicroseconds(tPerStep + a + 100);
          }
          break;
        }

        digitalWrite(right_stepper_STEP, HIGH);
        digitalWrite(left_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(right_stepper_STEP, LOW);
        digitalWrite(left_stepper_STEP, LOW);
        delayMicroseconds(tPerStep + a);
    }
    delay(10);
}


/**
 * @description: drives until it detects a line, no restriction
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
*/
void driveMaxUntilLine(unsigned int distanceOffset, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);    //set dir
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);      //short delay

    float a = 0;

    while(true) {
        getSensorValues();
        if(CoA) {
          if(dir == true && (usValues[1] < 30 || usValues[4] < 30 || usValues[6] < 20 || usValues[7] < 20)){
            break;
          }
          if(dir == false && (usValues[2] < 30 || usValues[5] < 30 || usValues[8] < 20 || usValues[9] < 20)){
            break;
          }
        }

        if((irValues[0] > 100) || (irValues[1] > 100) ||(irValues[2] > 100) || (irValues[3] > 100) || (irValues[4] > 100) || (irValues[5] > 100)) {
            break;
        }  
        //do a step
        digitalWrite(right_stepper_STEP, HIGH);
        digitalWrite(left_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(right_stepper_STEP, LOW);
        digitalWrite(left_stepper_STEP, LOW);
        delayMicroseconds(tPerStep + a);
        delay(10);
    }
}


/**
 * @description: drives until it hits the cherry support or drives against a wall
 * @param steps: steps to drive
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
*/
void driveUntilSwitch(unsigned int steps, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    float a = 0;

    for (unsigned int i = 0; i < steps; i++)
    {
        getSensorValues();
        a = 0;
        if(CoA) {
          if(dir == true && (usValues[1] < 30 || usValues[4] < 30 || usValues[6] < 20 || usValues[7] < 20)){
            break;
          }
          if(dir == false && (usValues[2] < 30 || usValues[5] < 30 || usValues[8] < 20 || usValues[9] < 20)){
            break;
          }
        }

        if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
            a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
        }

        if (steps - i < 300 && i > steps / 2){  // decelerate to make stopping smoother
            a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
        }

        if((digitalRead(left_limit_switch)  == LOW) && (digitalRead(right_limit_switch)  == LOW)){
          break;
        }

        digitalWrite(right_stepper_STEP, HIGH);
        digitalWrite(left_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(right_stepper_STEP, LOW);
        digitalWrite(left_stepper_STEP, LOW);
        delayMicroseconds(tPerStep + a);
    }
    delay(10);
}

/**
 * @description: drives until it hits the cherry support or drives against a wall
 * @param steps: steps to drive
 * @param dir: true(forward)/false(backward)
 * @param CoA: collision avoidance by US
*/
void driveMaxUntilSwitch(bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);



    float a = 0;

    while(true)
    {
        getSensorValues();
        a = 0;
        if(CoA) {
          if(dir == true && (usValues[1] < 30 || usValues[4] < 30 || usValues[6] < 20 || usValues[7] < 20)){
            break;
          }
          if(dir == false && (usValues[2] < 30 || usValues[5] < 30 || usValues[8] < 20 || usValues[9] < 20)){
            break;
          }
        }

        if((digitalRead(left_limit_switch)  == LOW) && (digitalRead(right_limit_switch)  == LOW)){
          break;
        }
        //do a step
        digitalWrite(right_stepper_STEP, HIGH);
        digitalWrite(left_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(right_stepper_STEP, LOW);
        digitalWrite(left_stepper_STEP, LOW);
        delayMicroseconds(tPerStep + a);
    }
    delay(10);
}


/**
 * @description: drives a set amount of mm
 * @param distance: distance in mm to drive
 * @param dir: true(forward)/false(backward)
 * @param condition: specifies a limit -> s(switch)/l(line)/n(nothing)
 * @param CoA: collision avoidance by US
*/
void driveDistanceMM(unsigned int distance, unsigned int distanceOffset, bool dir, char condition, bool CoA){         
    int steps = distance / outerWheelDistance * stepsPerRev;
    switch(condition){
      case 's':
        driveUntilSwitch(steps, dir, CoA);
        break;
      case 'l':
        driveUntilLine(steps, distanceOffset, dir, CoA);
        break;
      case 'n':
        driveFast(steps, dir, CoA);
        break;
    }
}

/**
 * @description: turns either right or left a given amount of steps
 * @param steps: amount of steps to turn
 * @param dir: true(clockwise)/false(counterclockwise)
*/
void turnFast(unsigned int steps, bool dir){        
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 1 : 0);

    delay(10);

    for (unsigned int i = 0; i < steps; i++)
    {
        digitalWrite(right_stepper_STEP, HIGH);
        digitalWrite(left_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep);
        digitalWrite(right_stepper_STEP, LOW);
        digitalWrite(left_stepper_STEP, LOW);
        delayMicroseconds(tPerStep);
    }
    delay(10);
}

/**
 * @description: rotates through a specified angle
 * @param degree: degrees to turn
 * @param dir: true(clockwise)/false(counterclockwise)
*/
void turnAngle(float degree, bool dir){
    distancePer360Rotation = 590.75;
        
    int steps = round(degree/360 *distancePer360Rotation / outerWheelDistance * stepsPerRev);
    turnFast(steps, dir);
}

void calibrationRoutine(){
    turnAngle(20,true);
    delay(500);

    while(millis() < 18000){
        turnAngle(40,false);
        delay(500);
        turnAngle(40,true);
        delay(500);
    }
    turnAngle(20,false);
}