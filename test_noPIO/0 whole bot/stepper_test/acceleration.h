#include <math.h>

/* data */
/**** Some constants ****/
const float wheelDiameter = 70;         // mm
const float outerWheelDistance = 223.0; // mm
const float wheelWidth = 20.00;         // mm
const float stepsPerRev = 1036 * 2;     // Steps that are needed to do one revolution

// Calculate the values for later calculating the amount of steps
const float midWheelDistance = outerWheelDistance - wheelWidth;
const float distancePerStep = PI * wheelDiameter / stepsPerRev;               // mm
const float anglePerStep = 360 / ((PI * outerWheelDistance) / distancePerStep); //°

#define tPerStep 550 // minimal delay between the steps in µs (For 11V: 873, For 22V: 436) function to calculate other Volts:f(x)=-39.727x + 1310

bool stop_driving = false;

/**
 * @description: drives either forward or backward a given amount of steps
 * @param steps: amount of steps to drive
 * @param dir: true(clockwise)/false(counterclockwise)
*/
void driveFast(unsigned int steps, bool dir){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    float a = 0; // for acceleration
    for (unsigned int i = 0; i < steps; i++)
    {
        a = 0;
        if (i < 400 && i <= steps / 2)           //acceleration section
        {
            a = tPerStep * 0.002 * (400 - i);
        }

        if (steps - i < 400 && i > steps / 2)    //deceleration section
        {
            a = tPerStep * 0.002 * (400 - (steps - i));
        }


        digitalWrite(left_stepper_STEP, HIGH);
        digitalWrite(right_stepper_STEP, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(left_stepper_STEP, LOW);
        digitalWrite(right_stepper_STEP, LOW);
        delayMicroseconds(tPerStep + a);
    }
    delay(10);
}

/**
 * @description: truns either right or left a given amount of steps
 * @param steps: amount of steps to turn
 * @param dir: true(clockwise)/false(counterclockwise)
*/
/*
void turnFast(unsigned int steps, bool dir){        
    digitalWrite(DIR_PIN_LEFT, dir ? 1 : 0);        
    digitalWrite(DIR_PIN_RIGHT, dir ? 1 : 0);
    digitalWrite(ENABLE_PIN, LOW);

    delay(10);

    float a = 0; // for acceleration
    for (unsigned int i = 0; i < steps; i++)
    {
        a = 0;
        if (i < 400 && i <= steps / 2)             //acceleration section
        {
            a = tPerStep * 0.003 * (400 - i);
        }

        if (steps - i < 400 && i > steps / 2)       //deceleration section
        {
            a = tPerStep * 0.003 * (400 - (steps - i));
        }

        digitalWrite(STEP_PIN, HIGH);
        delayMicroseconds(tPerStep + a);
        digitalWrite(STEP_PIN, LOW);
        delayMicroseconds(tPerStep + a);
    }

}
*/