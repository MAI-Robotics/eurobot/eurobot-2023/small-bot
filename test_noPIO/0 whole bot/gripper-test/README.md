# Gripper-System
- Wiring: not done yet
Folder for our Cherry-Gripping System. Since our first idea of the Vacuum-Cleaner didn't work, we moved on to another idea of grabbing the cherries using servo-motors.

## Images
![Gripper_Model](images/Gripper_Model.png)
![Gripper_Printed](images/Gripper_Printed.jpeg)

## 3D Models
- Main 3D Model ([Link](https://a360.co/3JjTvWj))
- Gripper Mount ([Link](https://a360.co/3WtMrJw))
- Gripper ([Left](https://a360.co/3wDeThH), [Right](https://a360.co/3D4dg06))